//
//  ViewController.swift
//  UAdivino
//
//  Created by Master Móviles on 3/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var reactionImage: UIImageView!
    
    var miAdivino = Adivino(nombre: "Rappel")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nameLabel.text = "Hola soy \(miAdivino.nombre)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onGetResponseClicked(_ sender: AnyObject) {
        let respuesta = miAdivino.obtenerRespuesta()
        
        responseLabel.text = respuesta.texto
        let image: UIImage? = respuesta.tipo ? UIImage(named: "si") : UIImage(named: "no")
        
        reactionImage.image = image
    }
}

